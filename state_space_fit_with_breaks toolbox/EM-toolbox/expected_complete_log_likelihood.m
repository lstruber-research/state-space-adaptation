function [likelihood] = expected_complete_log_likelihood(parameters,y,e,...
    SB,c,xnN,VnN,Vnp1nN)
% Author: Scott Albert
% Email: salbert8@jhu.edu
% Institution: Johns Hopkins University
% Lab: Laboratory for Computational Motor Control
% Advisor: Reza Shadmehr
% Date: July 25, 2017
% Location: Baltimore, MD 21211
% % Version: 1.1
%
% Summary: This function computes the expected complete log-likelihood.
%
% Notes: For more information about this package see README.pdf.
% 
% Input description:
%    parameters_0: the current estimate of the two state model parameters
%    y: the motor output on each trial
%    e: the error experienced by the subject on each trial
%    c: a model parameter that is assumed invariant
%    xnN: This is shorthand for the quantity x(n|N). It is the smoothed
%        Kalman state expectation  E[x(n)|y(1),y(2),...,y(N)].
%    VnN: This is shorthand for the quantity V(n|N). It is the smoothed
%        Kalman state variance  var(x(n)|y(1),y(2),...,y(N)).
%    Vnp1nN: This is shorthand for the quantity V(n+1,n|N). It is the
%        smoothed Kalman covariance of consecutive states, also written as
%        cov(x(n+1),x(n)|y(1),y(2),...,y(N)).
% 
% Output description:
%    likelihood: the expected complete log-likelihood
%
% Modification: Lucas Struber
% Email: lucas.struber@univ-grenoble-alpes.fr
% Institution: University Grenoble Alpes
% Lab: TIMC Laboratory
% Advisor: Fabien Cignetti
% Date: June 28, 2021
% % Version: 1.1b
% Summary of modifications: parameters extraction from parameters vector
% has been modified to handle multi-states models (instead of only
% two-state model)
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% allocations and compute matrix products %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% stores input variables using descriptive names
if mod(length(parameters)-4,3) ~= 0
    error("parameters must contains A, B and X0")
end
nbStates = (length(parameters)-4)/3;
    
A = diag(parameters(1:nbStates));
b = parameters(nbStates + 1:2*nbStates)';
x1 = parameters(2*nbStates + 1:3*nbStates)';
d = parameters(3*nbStates + 1);
sigmax2 = parameters(3*nbStates + 2);
sigmau2 = parameters(3*nbStates + 3);
sigma12 = parameters(3*nbStates + 4);

% sets the means and variances for the initial states
V1 = sigma12*eye(nbStates);

% sets matrices and vectors for the update of the fast and slow states
Q = sigmax2*eye(nbStates);

% a matrix for set break decay
D = A.^d;

% stores the number of trials
N = length(y);

% matrices for trials that are not set breaks
b1 = b;
A1 = A;
Q1 = Q;

% matrices for trials that are followed by set breaks
b2 = D*b;
A2 = D*A;
Q2 = D*Q*D';

% precomputes important quantities for trials that are not followed by a
% set break
QinvA1 = Q1\A1; Qinvb1 = Q1\b1; AtQinv1 = (A1')/Q1; AtQinvA1 = AtQinv1*A1;
AtQinvb1 = (A1')*Qinvb1; btQinv1 = (b1')/Q1; btQinvA1 = btQinv1*A1;
btQinvb1 = btQinv1*b1;

% precomputes important quantities for trials are not followed by a
% set break
QinvA2 = Q2\A2; Qinvb2 = Q2\b2; AtQinv2 = (A2')/Q2; AtQinvA2 = AtQinv2*A2;
AtQinvb2 = (A2')*Qinvb2; btQinv2 = (b2')/Q2; btQinvA2 = btQinv2*A2;
btQinvb2 = btQinv2*b2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% compute the likelihood %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In this section the expected complete log-likelihood is computed in
% different parts.

% TERM 1: one of the parts of the expected complete log-likelihood function
% derived from the likelihood of observing the motor output given the
% states
term1 = 0;
for n = 1 : N
    term1 = term1 + y(n)^2 + (c')*(VnN{n} + xnN{n}*(xnN{n}'))*c - ...
        2*y(n)*(c')*xnN{n};
end
term1 = -term1/(2*sigmau2);

% TERM 2: one of the parts of the expected complete log-likelihood function
% derived from the likelihood of observing the state on trial n+1 given the
% state on trial n
term2 = 0;
for n = 1 : N - 1
    % determines if this trial is followed by a set break
    if SB(n) == 0
        % this trial is not followed by a set break
        Q = Q1; QinvA = QinvA1; Qinvb = Qinvb1; AtQinv = AtQinv1;
        AtQinvA = AtQinvA1; AtQinvb = AtQinvb1; btQinv = btQinv1;
        btQinvA = btQinvA1; btQinvb = btQinvb1;
    else
        % this trial is followed by a set break
        Q = Q2; QinvA = QinvA2; Qinvb = Qinvb2; AtQinv = AtQinv2;
        AtQinvA = AtQinvA2; AtQinvb = AtQinvb2; btQinv = btQinv2;
        btQinvA = btQinvA2; btQinvb = btQinvb2;
    end
    
    term2 = term2 + (xnN{n+1}')*(Q\xnN{n+1}) + trace(Q\VnN{n+1}) - ...
        (xnN{n+1}')*QinvA*xnN{n} - trace(QinvA*(Vnp1nN{n}')) - ...
        (xnN{n+1}')*Qinvb*e(n) - (xnN{n}')*AtQinv*xnN{n+1} - ...
        trace(AtQinv*Vnp1nN{n}) + (xnN{n}')*AtQinvA*xnN{n} + ...
        trace(AtQinvA*VnN{n}) + (xnN{n}')*AtQinvb*e(n) - ...
        e(n)*btQinv*xnN{n+1} + e(n)*btQinvA*xnN{n} + e(n)*btQinvb*e(n);
end
term2 = -term2/2;

% TERM 3: one of the parts of the expected complete log-likelihood function
% derived from the likelihood of observing the initial state
term3 = (xnN{1}')*(V1\xnN{1}) + trace(V1\VnN{1}) - (xnN{1}')*(V1\x1) - ...
    (x1')*(V1\xnN{1}) + (x1')*(V1\x1);
term3 = -term3/2;

% TERM 4: one of the parts of the expected complete log-likelihood function
% derived from the pre-exponential factors
term4 = -(1/2)*log(det(V1)) - (N/2)*log(sigmau2) - (3/2)*N*log(2*pi);

% TERM 5: one of the parts of the expected complete log-likelihood function
% derived from the pre-exponential factors of x(n+1) given x(n) 
term5 = 0;
for n = 1 : N-1
    % determines if this trial is followed by a set break
    if SB(n) == 0
        % this trial is not followed by a set break
        Q = Q1;
    else
        % this trial is followed by a set break
        Q = Q2;
    end
    term5 = term5 + log(det(Q));
end
term5 = -term5/2;

% computes the likelihood from the sum of all terms
likelihood = term1 + term2 + term3 + term4 + term5;
