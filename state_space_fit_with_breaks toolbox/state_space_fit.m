function [output, params, break_decay, asymptote, noises_var, states, performances] = ...
    state_space_fit(behavior, perturb, breaks, params_init, params_search_ranges, fit_method, search_method)
% Author: Lucas Struber
% Email: lucas.struber@univ-grenoble-alpes.fr
% Institution: University Grenoble Alpes
% Lab: TIMC Laboratory
% Advisor: Fabien Cignetti
% Date: June 28, 2021
% Version: 1.0
%
% Summary: fit a state-space model to experimental data
%
% Input description:
%    behavior: the motor output on each trial.
%       size = nbTrials x 1
%    perturb: the perturbation on each trial.
%       size = nbTrials x 1
%    params_init: an initial guess of the parameters that seed the
%       optimization algorithm, and that also define the number of states
%       of the model - 3 parameters for a one-state model [A,b,x0] and
%       6 for a two-state model [As, bs, x0s; Af, bf, x0f].
%       size = nbStates x 3 
%    params_search_ranges: a matrix containing upper and lower bounds for 
%       the model parameters  - 6 bounds for a one-state model [Amin, Amax,
%       bmin, bmax, x0min, x0max] and 12 for a two-states model [Asmin, 
%       Asmax, bsmin, bsmax, x0smin, x0smax; Afmin, Afmax, bfmin, bfmax,
%       x0fmin, x0fmax].
%       size = nbStates x 6
%    fit_method: a string that define the optimization algorithm that is
%       used (and the stochastic/deterministic nature of the modeling).
%       possible values: 'lmse' or 'em' (i.e. least mean square error
%       estimator or expectation-maximization algorithm)
%       Credits: provided EM toolbox has been adapted from Scott Albert
%       toolbox avalaible at http://shadmehrlab.org/tools
%    search_method: a string that define the form of the search space for 
%       optimization algorithm.
%       possible values: 'norm' or 'log'. If set to "log", it considers
%       that provided parameters must be converted in the 0-1 log space.
%
% Output description:
%    output: model output (sum of states)
%       size = nbTrials x 1
%    params: optimized set of parameters
%       size = nbStates x 3
%    asymptote: predicted asymptote of the model
%       asymptote = ones(1,nbStates)*((eye(nbStates)-(diag(A)-b'*...
%       ones(1,nbStates)))^(-1))*b'*mean(perturb);
%       size = 1 x 1
%    noises_var: extracted noises of the model. If fit_method is 'lmse',
%       only one noise is considered, between model and measurements
%       (sigmam), and if fit_method is em, three noises are optimized 
%       through Kalman filter and MLE procedure, sigmau, sigmax (motor
%       noise and state noise variances)
%       size = nbStates x 3
%    states: states' vector. In one-state model, states = output.
%       size = nbTrials x nbStates
%    performances: model performances, characterized by two classic
%       indicators: AICc (corrected Akaike information criteria) and MSE
%       (mean square error)
%       size = 1 x 2

    addpath(strcat(fileparts(which('state_space_fit.m')),'\EM-toolbox'))
    
    % function need at least the behavior and the perturbation
    if nargin < 3
        error('error: this function need at least the behavior, perturbation and set breaks vectors');
    end

    % default value if initial guess is not specified (implies one state model)
    if nargin < 4
        params_init = [0.9, 0.1, 0];
    end

    % retrieve number of states from initial guess
    nbStates = size(params_init,1);
    if nbStates >= 3
        error('error: this function only deals with state-spaces models with one or two states');
    end

    if size(params_init,2) < 2
        error('error: params_init must contain at least A and b initial guess for each state');
        % if x0_init is not specified, set it to 0
    elseif size(params_init,2) < 3
        for s = 1:nbStates
            params_init(s,3) = 0;
        end
    end

    % default value for search ranges (0 < A < 1, 0 < B < 1 and X0 = 0)
    if nargin < 5
        params_search_ranges = [zeros(nbStates,1), ones(nbStates,1), ... % Amin - Amax
            zeros(nbStates,1), ones(nbStates,1),...                      % Bmin - Bmax
            zeros(nbStates,1), zeros(nbStates,1)];                       % x0min - x0max
    end
    if size(params_search_ranges,1) ~= nbStates
        error('error: the size of params_search_ranges is not compatible with the number of states (defined from params_init)');
    end
    if size(params_search_ranges,2) < 4
        error('error: params_search_ranges must contain at least Amin, Amax, bmin and bmax for each state');
        % if x0 search range is not specified, set it to 0
    elseif size(params_search_ranges,2) < 6
        for s = 1:nbStates
            params_search_ranges(s,5) = 0;
            params_search_ranges(s,6) = 0;
        end
    end

    % default fit method
    if nargin < 6
        fit_method = 'lmse';
    end

    % default search space
    if nargin < 7
        search_method = 'norm';
    end

    % check that string params are correct
    if not(strcmp(fit_method,'lmse') || strcmp(fit_method,'em'))
        warning('unrecognized fit method: set fit method to lmse')
        fit_method = 'lmse';
    end

    if not(strcmp(search_method,'norm') || strcmp(search_method,'log'))
        warning('unrecognized search method: set search method to norm')
        search_method = 'norm';
    end

    if strcmp(search_method,'log')
%         params_init(:,1:2) = -log(1./params_init(:,1:2) - 1);
%         params_search_ranges(:,1:4) = -log(1./params_search_ranges(:,1:4) - 1);
%         
%         params_init(isinf(params_init)) = sign(params_init(isinf(params_init)))*10;
%         params_search_ranges(isinf(params_search_ranges)) = sign(params_search_ranges(isinf(params_search_ranges)))*10;
        warning('search method: search method set to norm')
        search_method = 'norm';
    end

    % number of parameters optimized (for AIC computation) + constraints
    % initialization
    nbOptParams = 0;
    
    if nbStates == 2 
        A_con = zeros(2,6);
        b_con = zeros(2,1); 
    end
    
    for s = 1:nbStates
        if(params_search_ranges(s,1) ~= params_search_ranges(s,2))
            nbOptParams = nbOptParams + 1;
            if s == 2
                % As >= Af + step
                A_con(1,:) = [-1,1,0,0,0,0];
                if strcmp(fit_method,'lmse')
                    b_con(1,1) = -0.001;
                else
                    b_con(1,1) = -0.01;
                end
            end
        end
        if(params_search_ranges(s,3) ~= params_search_ranges(s,4))
            nbOptParams = nbOptParams + 1;
            if s == 2
                % Bf >= Bs + step
                A_con(2,:) = [0,0,1,-1,0,0];
                if strcmp(fit_method,'lmse')
                    b_con(2,1) = -0.001;
                else
                    b_con(2,1) = -0.01;
                end
            end
        end
        if(params_search_ranges(s,5) ~= params_search_ranges(s,6))
            nbOptParams = nbOptParams + 1;
        end
    end
    if strcmp(fit_method,'lmse')
        nbOptParams = nbOptParams + 1; % sigma m
    else
        nbOptParams = nbOptParams + 2; % sigma u and sigma x
    end

    % local function to be used by fmincon that compute the mean square error
    % of a deterministic state-space given a set of parameters
    function mse = deterministic_ss_mse(S)
        model_estimate = simulate_deterministic_state_space(S,perturb,behavior,breaks,search_method);
        mse = mean((model_estimate - behavior).^2);
    end

    if strcmp(fit_method,'lmse')
        if(nbStates == 1)
            S_opt = fmincon(@deterministic_ss_mse,[params_init, 8],[],[],[],[],[params_search_ranges(1,[1 3 5]),0.1],[params_search_ranges(1,[2 4 6]),30]);
        elseif(nbStates == 2)
            S_opt = fmincon(@deterministic_ss_mse,[reshape(params_init,1,3*nbStates),8],A_con,b_con,[],[],[reshape(params_search_ranges(:,[1 3 5]),1,nbStates*3),0.1],[reshape(params_search_ranges(:,[2 4 6]),1,nbStates*3),30]);
        end
        
        [output, states] = simulate_deterministic_state_space(S_opt,perturb,behavior,breaks,search_method);

        noises_var = var(output - behavior);
        mse_opt = mean((output - behavior).^2);
        lik_opt = (length(behavior)/2)*(-mse_opt/noises_var-log(noises_var)-log(2*pi));
        
    
    elseif strcmp(fit_method,'em')
        if(nbStates == 1)
            [S_opt, likelihoods] = generalized_expectation_maximization(...
            [reshape(params_init,1,3*nbStates), 8, 2, 2, 5],... % Initial guess
            behavior,perturb,zeros(length(behavior),1),nan(length(behavior),1),breaks,ones(nbStates,1),... % Paradigm
            [reshape(params_search_ranges(:,[1 3 5]),1,nbStates*3)', reshape(params_search_ranges(:,[2 4 6]),1,nbStates*3)'; 0.1,30; 0.0001,10000; 0.0001,10000; 0.0000001,10],...% Search-space
            [],[],... % constraints
            100,search_method);
        elseif(nbStates == 2)                 
            [S_opt, likelihoods] = generalized_expectation_maximization(...
            [reshape(params_init,1,3*nbStates), 8, 2, 2, 5],... % Initial guess
            behavior,perturb,zeros(length(behavior),1),nan(length(behavior),1),breaks,ones(nbStates,1),... % Paradigm
            [reshape(params_search_ranges(:,[1 3 5]),1,nbStates*3)', reshape(params_search_ranges(:,[2 4 6]),1,nbStates*3)'; 0.1, 30; 0.0001,10000; 0.0001,10000; 0.0000001,10],...% Search-space
            A_con,b_con,... % constraints
            100,search_method);
        end
        
        [output, states] = simulate_deterministic_state_space(S_opt(1:3*nbStates+1),perturb,behavior,breaks,search_method);

        mse_opt = mean((output - behavior).^2);
        lik_opt = max(likelihoods);
        noises_var = S_opt(3*nbStates + 2:3*nbStates + 3);
    end

    AICc = 2*nbOptParams - 2*lik_opt + (2*nbOptParams*(nbOptParams+1))/(length(behavior)-nbOptParams-1);
    performances = [AICc, mse_opt];

    A = S_opt(1:nbStates);
    b = S_opt(nbStates + 1:2*nbStates);
    x0 = S_opt(2*nbStates + 1:3*nbStates);
    break_decay = S_opt(3*nbStates + 1);
    
%     if strcmp(search_method,'log')
%         A = 1./(1+exp(-A));
%         b = 1./(1+exp(-b));
%     end

    asymptote = ones(1,nbStates)*((eye(nbStates)-(diag(A)-b'*ones(1,nbStates)))^(-1))*b'*mean(perturb);
    params = [A';b';x0'];
end

