function [y_pred, state] = simulate_deterministic_state_space(S,perturb,behavior,breaks,search_method)
% Author: Lucas Struber
% Email: lucas.struber@univ-grenoble-alpes.fr
% Institution: University Grenoble Alpes
% Lab: TIMC Laboratory
% Advisor: Fabien Cignetti
% Date: June 28, 2021
% Version: 1.0
%
% Summary: simulate a state-space model ignoring noises from a set of
% parameters
%
% Input description:
%    S: set of parameters [a1, ..., as, b1, ... bs, x01, ..., x0s] where s
%       is the number of states
%    behavior: the motor output on each trial
%    perturb: the perturbation on each trial
%    search_space: a string "norm" or "log". If set to "log", it considers
%       that provided parameters are in the 0-1 log space
%
% Output description:
%    y_pred: model output (sum of states)
%    state: states' vector
    if mod(length(S)-1,3) ~= 0
        error("S must contains A, B and X0")
    end
    nbStates = (length(S)-1)/3;

    A = S(1:nbStates);
    b = S(nbStates + 1:2*nbStates);
    X0 = S(2*nbStates + 1:3*nbStates);
    d = S(3*nbStates + 1);
    D = A.^d;
    
    b1 = b;
    A1 = A;

    b2 = (diag(D)*b')';
    A2 = D.*A;
%     if strcmp(search_method,'log')
%         A = 1./(1+exp(-A));
%         b = 1./(1+exp(-b));
%     end

    y_pred = zeros(size(behavior));

    state = zeros(length(behavior),nbStates);

    for s = 1:nbStates
        state(1,s) = X0(s);
    end
    y_pred(1) = sum(state(1,:));

    for t = 2:length(behavior)
        if breaks(t) == 0
            % this trial is not followed by a set break
            A = A1; b = b1;
        else
            % this trial is followed by a set break
            A = A2; b = b2;
        end
        
        for s = 1:nbStates
            state(t,s) = A(s)*state(t-1,s) + b(s)*(perturb(t) - y_pred(t-1));
        end

        y_pred(t) = sum(state(t,:));
    end
end