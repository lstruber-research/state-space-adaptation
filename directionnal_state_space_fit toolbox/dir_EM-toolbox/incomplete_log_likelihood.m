function [likelihood] = dir_incomplete_log_likelihood(y,e,c,parameters,search_method)
% Author: Scott Albert
% Email: salbert8@jhu.edu
% Institution: Johns Hopkins University
% Lab: Laboratory for Computational Motor Control
% Advisor: Reza Shadmehr
% Date: July 25, 2017
% Location: Baltimore, MD 21211
% % Version: 1.1
%
% Summary: This function computes the incomplete log-likelihood function.
%    The EM algorithm attempts to increase the value of this function each
%    iteration. It is the function maximized in standard MLE.
%
% Notes: For more information about this package see README.pdf.
%
% Input description:
%    y: the motor output on each trial
%    e: the error experienced by the subject on each trial
%    c: a model parameter that is assumed invariant
%    parameters: a two state model parameter set
%
% Output description:
%    likelihood: the incomplete log-likelihood for this parameter set
%
% Modification: Lucas Struber
% Email: lucas.struber@univ-grenoble-alpes.fr
% Institution: University Grenoble Alpes
% Lab: TIMC Laboratory
% Advisor: Fabien Cignetti
% Date: August 20, 2021
% % Version: 1.1b_dir
% Summary of modifications (from v1.1b):
%   Modifications aimed to handle multi-target models where states
%   interacts with each other (B is a matrix), but contrary to version
%   1.1b, adaptation rates are similar between states (each line of B is a
%   circular rotation of another line). This means that degree of
%   trial-by-trial adaptation depends only on the angular difference
%   between targets. As well, only one retention factor is provided in
%   parameters assuming it is the same across targets. These modifications
%   include:
%   1. parameters extraction from parameters vector has been modified
%   2. c model parameter is now trial-dependant to handle an output that
%   take into account a different state at each step
%
%   Note that version 1.1b and 1.1b_dir could be gathered into a single
%   version handling general multi-states model with interaction terms
%   where A and B are matrices of nbStates*nbStates parameters, however
%   constraints on A and B (e.g. for multi-target model diagonal and
%   subdiagonal etc. terms of B are equal) would be too difficult to write
%   for the end user.

% stores input variables using descriptive names
if mod(length(parameters)-4,2) ~= 0
    error("parameters must contains A, B and X0")
end
nbTargets = (length(parameters)-4)/2;

if strcmp(search_method,'log')
    parameters(1:nbTargets+1) = 1./(1+exp(-parameters(1:nbTargets+1))); % A and b are log
end

A = parameters(1,1)*eye(nbTargets);
b = parameters(2:nbTargets+1)';
b_mat = zeros(nbTargets,nbTargets);
for tg = 1:nbTargets
    b_mat(tg,:) = circshift(b,tg-1);
end
    
x1 = parameters(nbTargets + 2:2*nbTargets + 1)';
    
sigmax2 = parameters(2*nbTargets + 2);
sigmau2 = parameters(2*nbTargets + 3);
sigma12 = parameters(2*nbTargets + 4);

% sets the means and variances for the initial states
V1 = sigma12*eye(nbTargets);

% sets matrices and vectors for the update of the fast and slow states
Q = sigmax2*eye(nbTargets);

% determines the number of trials
N = length(y);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% forward Kalman filter %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In this section, a forward Kalman filter is used to compute prior and
% posterior expectations and variances of the state x.
%
% Notation:
%    the posterior expectation E(x(n)|y(1),...,y(n)) is denoted xnn
%    the posterior variance var(x(n)|y(1),...,y(n)) is denoted Vnn
%    the prior expectation E(x(n)|y(1),...,y(n-1)) is denoted xnnm1
%    the prior variance var(x(n)|y(1),...,y(n-1)) is denoted Vnnm1

% allocates space for arrays for the prior and posterior expectations and
% variances of the hidden states
xnnm1 = cell(N,1);
Vnnm1 = cell(N,1);
xnn = cell(N,1);
Vnn = cell(N,1);

% specifies the initial prior, x(1|0) = x1
xnnm1{1} = x1;

% specifies the initial prior variance, V(1|0) = V1
Vnnm1{1} = V1;

% the standard forward Kalman filter
for n = 1 : N    
    % computes the Kalman gain
    k = (Vnnm1{n}*c(:,n)) / ((c(:,n)')*Vnnm1{n}*c(:,n) + sigmau2);
    
    % computes the error between our actual and predicted y values
    y_error = y(n) - (c(:,n)')*xnnm1{n};
    
    % computes the posterior state expectation
    xnn{n} = xnnm1{n} + k*y_error;
    
    % computes the posterior state variance
    Vnn{n} = (eye(nbTargets) - k*(c(:,n)'))*Vnnm1{n};    
    
    % forward projects, unless the last trial has been reached
    if n < N        
        % computes the next prior state
        xnnm1{n+1} = A*xnn{n} + b_mat*c(:,n)*e(n);
        
        % computes the next prior variance
        Vnnm1{n+1} = A*Vnn{n}*(A') + Q;
    end
end

% computes the log-likelihood, log[L(y(1),y(2),...y(N)|parameters)]
likelihood = -(N/2)*log(2*pi);
for n = 1 : N
    % the variance and mean of the normal random variable
    SIGMA = (c(:,n)')*Vnnm1{n}*c(:,n) + sigmau2;
    MU = (c(:,n)')*xnnm1{n};
    
    % updates the likelihood
    likelihood = likelihood - (1/2)*log(SIGMA) - ...
        (1/2) * ( (y(n) - MU)^2 ) / SIGMA;
end