function [parameters_final] = m_step(parameters_0,xnN,VnN,Vnp1nN,y,e,c,...
    search_space,search_method)
% Author: Scott Albert
% Email: salbert8@jhu.edu
% Institution: Johns Hopkins University
% Lab: Laboratory for Computational Motor Control
% Advisor: Reza Shadmehr
% Date: July 25, 2017
% Location: Baltimore, MD 21211
% % Version: 1.1
%
% Summary: This function uses fmincon to perform the M-step of the EM
%    algorithm. The parameter set that maximizes the expected complete
%    log-likelihood function in a constrained parameter space is identified
%    using fmincon.
%
% Notes: For more information about this package see README.pdf.
%
% Input description:
%    parameters_0: the current estimate of the two state model parameters
%    xnN: This is shorthand for the quantity x(n|N). It is the smoothed
%        Kalman state expectation  E[x(n)|y(1),y(2),...,y(N)].
%    VnN: This is shorthand for the quantity V(n|N). It is the smoothed
%        Kalman state variance  var(x(n)|y(1),y(2),...,y(N)).
%    Vnp1nN: This is shorthand for the quantity V(n+1,n|N). It is the
%        smoothed Kalman covariance of consecutive states, also written as
%        cov(x(n+1),x(n)|y(1),y(2),...,y(N)).
%    y: the motor output on each trial
%    e: the error experienced by the subject on each trial
%    c: a model parameter that is assumed invariant
%    search_space: a matrix containing upper and lower bounds for the model
%        parameters
%    constraints: an array that specifies linear inequality constraints
%        between the fast and slow retention factors, and error
%        sensitivities
%    use_mex: determines if an mex function will be used or a regular
%        MATLAB function (m-file)
%
% Output description:
%    parameters_final: the parameter set identified by fmincon that
%        maximizes the expected complete log-likelihood function
%
% Modification: Lucas Struber
% Email: lucas.struber@univ-grenoble-alpes.fr
% Institution: University Grenoble Alpes
% Lab: TIMC Laboratory
% Advisor: Fabien Cignetti
% Date: August 20, 2021
% % Version: 1.1b_dir
% Summary of modifications (from v1.1b):
%   Modifications aimed to handle multi-target models where states
%   interacts with each other (B is a matrix), but contrary to version
%   1.1b, adaptation rates are similar between states (each line of B is a
%   circular rotation of another line). This means that degree of
%   trial-by-trial adaptation depends only on the angular difference
%   between targets. As well, only one retention factor is provided in
%   parameters assuming it is the same across targets. These modifications
%   include:
%   1. parameters extraction from parameters vector has been modified
%   2. c model parameter is now trial-dependant to handle an output that
%   take into account a different state at each step
%
%   Note that version 1.1b and 1.1b_dir could be gathered into a single
%   version handling general multi-states model with interaction terms
%   where A and B are matrices of nbStates*nbStates parameters, however
%   constraints on A and B (e.g. for multi-target model diagonal and
%   subdiagonal etc. terms of B are equal) would be too difficult to write
%   for the end user.

% stores input variables using descriptive names
if mod(length(parameters_0)-4,2) ~= 0
    error("parameters must contains A, B and X0")
end
nbTargets = (length(parameters_0)-4)/2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%% Call to the likelihood function %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% creates a local function to be used by fmincon that computed the negated
% value of the expected complete log-likelihood function
function negated_likelihood = likelihood_function(x)
    
    if strcmp(search_method,'log')
        x(1:nbTargets+1) = 1./(1+exp(-x(1:nbTargets)));
    end
        
    % calls a MATLAB function to compute the likelihood
    likelihood = expected_complete_log_likelihood(x,y,e,c,xnN,VnN,Vnp1nN);
        
    % negates the likelihood
    negated_likelihood = -likelihood;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% set up and perform fmincon %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% resets options for fmincon
options = optimoptions('fmincon');
options.Display = 'off';

% specifies the lower and upper bounds for the fmincon search
lb = search_space(:,1);
ub = search_space(:,2);

parameters_final = fmincon(@likelihood_function, parameters_0, [], [], ...
    [], [], lb, ub, [], options);
end