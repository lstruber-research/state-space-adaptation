function [likelihood] = dir_expected_complete_log_likelihood(parameters,y,e,...
    c,xnN,VnN,Vnp1nN)
% Author: Scott Albert
% Email: salbert8@jhu.edu
% Institution: Johns Hopkins University
% Lab: Laboratory for Computational Motor Control
% Advisor: Reza Shadmehr
% Date: July 25, 2017
% Location: Baltimore, MD 21211
% % Version: 1.1
%
% Summary: This function computes the expected complete log-likelihood.
%
% Notes: For more information about this package see README.pdf.
% 
% Input description:
%    parameters_0: the current estimate of the two state model parameters
%    y: the motor output on each trial
%    e: the error experienced by the subject on each trial
%    c: a model parameter that is assumed invariant
%    xnN: This is shorthand for the quantity x(n|N). It is the smoothed
%        Kalman state expectation  E[x(n)|y(1),y(2),...,y(N)].
%    VnN: This is shorthand for the quantity V(n|N). It is the smoothed
%        Kalman state variance  var(x(n)|y(1),y(2),...,y(N)).
%    Vnp1nN: This is shorthand for the quantity V(n+1,n|N). It is the
%        smoothed Kalman covariance of consecutive states, also written as
%        cov(x(n+1),x(n)|y(1),y(2),...,y(N)).
% 
% Output description:
%    likelihood: the expected complete log-likelihood
%
% Modification: Lucas Struber
% Email: lucas.struber@univ-grenoble-alpes.fr
% Institution: University Grenoble Alpes
% Lab: TIMC Laboratory
% Advisor: Fabien Cignetti
% Date: August 20, 2021
% % Version: 1.1b_dir
% Summary of modifications (from v1.1b):
%   Modifications aimed to handle multi-target models where states
%   interacts with each other (B is a matrix), but contrary to version
%   1.1b, adaptation rates are similar between states (each line of B is a
%   circular rotation of another line). This means that degree of
%   trial-by-trial adaptation depends only on the angular difference
%   between targets. As well, only one retention factor is provided in
%   parameters assuming it is the same across targets. These modifications
%   include:
%   1. parameters extraction from parameters vector has been modified
%   2. c model parameter is now trial-dependant to handle an output that
%   take into account a different state at each step
%
%   Note that version 1.1b and 1.1b_dir could be gathered into a single
%   version handling general multi-states model with interaction terms
%   where A and B are matrices of nbStates*nbStates parameters, however
%   constraints on A and B (e.g. for multi-target model diagonal and
%   subdiagonal etc. terms of B are equal) would be too difficult to write
%   for the end user.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% allocations and compute matrix products %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% stores input variables using descriptive names
if mod(length(parameters)-4,2) ~= 0
    error("parameters must contains A, B and X0")
end
nbTargets = (length(parameters)-4)/2;

A = parameters(1,1)*eye(nbTargets);
b = parameters(2:nbTargets+1)';
b_mat = zeros(nbTargets,nbTargets);
for tg = 1:nbTargets
    b_mat(tg,:) = circshift(b,tg-1);
end
    
x1 = parameters(nbTargets + 2:2*nbTargets + 1)';
    
sigmax2 = parameters(2*nbTargets + 2);
sigmau2 = parameters(2*nbTargets + 3);
sigma12 = parameters(2*nbTargets + 4);

% sets the means and variances for the initial states
V1 = sigma12*eye(nbTargets);

% sets matrices and vectors for the update of the fast and slow states
Q = sigmax2*eye(nbTargets);

% stores the number of trials
N = length(y);

% precomputes important quantities referenced in the expected complete
% log-likelihood function
QinvA = Q\A; AtQinv = (A')/Q; AtQinvA = AtQinv*A;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% compute the likelihood %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In this section the expected complete log-likelihood is computed in
% different parts.

% TERM 1: one of the parts of the expected complete log-likelihood function
% derived from the likelihood of observing the motor output given the
% states
term1 = 0;
for n = 1 : N
    term1 = term1 + y(n)^2 + (c(:,n)')*(VnN{n} + xnN{n}*(xnN{n}'))*c(:,n) - ...
        2*y(n)*(c(:,n)')*xnN{n};
end
term1 = -term1/(2*sigmau2);

% TERM 2: one of the parts of the expected complete log-likelihood function
% derived from the likelihood of observing the state on trial n+1 given the
% state on trial n
term2 = 0;
for n = 1 : N - 1
    Qinvb = Q\(b_mat*c(:,n)); AtQinvb = (A')*Qinvb; btQinv = ((b_mat*c(:,n))')/Q; btQinvA = btQinv*A;
    btQinvb = btQinv*(b_mat*c(:,n));

    term2 = term2 + (xnN{n+1}')*(Q\xnN{n+1}) + trace(Q\VnN{n+1}) - ...
        (xnN{n+1}')*QinvA*xnN{n} - trace(QinvA*(Vnp1nN{n}')) - ...
        (xnN{n+1}')*Qinvb*e(n) - (xnN{n}')*AtQinv*xnN{n+1} - ...
        trace(AtQinv*Vnp1nN{n}) + (xnN{n}')*AtQinvA*xnN{n} + ...
        trace(AtQinvA*VnN{n}) + (xnN{n}')*AtQinvb*e(n) - ...
        e(n)*btQinv*xnN{n+1} + e(n)*btQinvA*xnN{n} + e(n)*btQinvb*e(n);
end
term2 = -term2/2;

% TERM 3: one of the parts of the expected complete log-likelihood function
% derived from the likelihood of observing the initial state
term3 = (xnN{1}')*(V1\xnN{1}) + trace(V1\VnN{1}) - (xnN{1}')*(V1\x1) - ...
    (x1')*(V1\xnN{1}) + (x1')*(V1\x1);
term3 = -term3/2;

% TERM 4: one of the parts of the expected complete log-likelihood function
% derived from the pre-exponential factors
term4 = -(1/2)*log(det(V1)) - (N/2)*log(sigmau2) - (3/2)*N*log(2*pi);

% TERM 5: one of the parts of the expected complete log-likelihood function
% derived from the pre-exponential factors of x(n+1) given x(n)
term5 = -(N-1)*log(det(Q))/2;

% computes the likelihood from the sum of all terms
likelihood = term1 + term2 + term3 + term4 + term5;
