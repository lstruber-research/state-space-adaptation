% load behavioral data
clear all, close all, clc;
load('example_behavior')
behavior = perturb - measured_error;

% Parameters initialization
%                        a0   b0   x00
params_init_Diedrischen   = [   1,  0.1,  0];
params_init_Albert = [0.95, 0.05,  0;...
                        0.7,  0.3,  0];
%                       amin amax bmin bmax x0min x0max
search_space_Diedrischen   = [   1,   1,   0, 0.5,  -15, 15];
search_space_Albert = [ 0.2,   1,   0, 0.5,    0,  0;... % slow
                         0.2,   1,   0, 0.5,  -15, 15];   % fast

% loop on the number of subjects
nbSub = size(measured_error,2);
nbTrials = size(measured_error,1);
% init fit outputs
output_Diedrischen = zeros(nbTrials,nbSub);
params_Diedrischen = zeros(3,nbSub); % 3 parameters, A, n, x0
asymptote_Diedrischen= zeros(1,nbSub);
noise_Diedrischen = zeros(1,nbSub); % 1 noise (LMSE approach), sigma m
perf_Diedrischen = zeros(2,nbSub); % 2 performances indicators, AICc & MSE

output_Albert = zeros(nbTrials,nbSub);
params_Albert = zeros(6,nbSub); % 6 parameters, As, Af, bs, bf, x0s, x0f
asymptote_Albert = zeros(1,nbSub);
noise_Albert = zeros(3,nbSub); % 3 noises (EM algorithm), sigmau, sigmax and sigma12
internalStates_Albert = zeros(nbTrials,2,nbSub); % 2 states model
perf_Albert = zeros(2,nbSub); % 2 performances indicators, AICc & MSE

for s = 1:nbSub
    behavior(:,s) = smooth(behavior(:,s),2); % eventually interpolate missing values

    % Fit a one-state model without retention factor (Diedrieschen, 2003),
    % using a deterministic lmse approach
    [output_Diedrischen(:,s), params_Diedrischen(:,s), asymptote_Diedrischen(:,s), noise_Diedrischen(:,s), ~, perf_Diedrischen(:,s)] = ...
        state_space_fit(behavior(:,s), perturb, params_init_Diedrischen, search_space_Diedrischen, 'lmse', 'norm');

    % Fit a two-state model with retention factor using stochastic EM approach
    % (Albert, 2018) optimizing parameters in log-space
    [output_Albert(:,s), params_Albert(:,s), asymptote_Albert(:,s), noise_Albert(:,s), internalStates_Albert(:,:,s), perf_Albert(:,s)] = ...
        state_space_fit(behavior, perturb, params_init_TwoSave, search_space_TwoSave, 'em', 'log');


    figure();
    hold on;
    plot(behavior, 'LineWidth',1);
    plot(output_Diedrischen(:,s), 'LineWidth',2);
    legend('Observed error','Model output');
    hold off;
    ylabel('Angle (�)');
    xlabel('Trials');

    FigName = strcat('Sub : ', num2str(s));
    figure('Name',FigName);
    hold on;
    plot(behavior, 'LineWidth',1);
    plot(output_Albert(:,s),'LineWidth',2);
    plot(internalStates_Albert(:,1,s),'LineStyle','--','LineWidth',1);
    plot(internalStates_Albert(:,2,s),'LineStyle','-.','LineWidth',1);
    legend('Observed error','Model prediction','Slow-state','Fast-state');
    hold off;
    ylabel('Angle (�)');
    xlabel('Trials');
end