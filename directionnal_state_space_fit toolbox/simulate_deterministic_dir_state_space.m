function [y_pred, state] = simulate_deterministic_dir_state_space(A,b,X0,perturb,behavior,target,search_space)
% Author: Lucas Struber
% Email: lucas.struber@univ-grenoble-alpes.fr
% Institution: University Grenoble Alpes
% Lab: TIMC Laboratory
% Advisor: Fabien Cignetti
% Date: June 28, 2021
% Version: 1.0
%
% Summary: simulate a state-space model ignoring noises from a set of
% parameters
%
% Input description:
%    S: set of parameters [a1, ..., as, b1, ... bs, x01, ..., x0s] where s
%       is the number of states
%    behavior: the motor output on each trial
%    perturb: the perturbation on each trial
%    search_space: a string "norm" or "log". If set to "log", it considers
%       that provided parameters are in the 0-1 log space
%
% Output description:
%    y_pred: model output (sum of states)
%    state: states' vector

    if strcmp(search_space,'log')
        A = 1./(1+exp(-A));
        b = 1./(1+exp(-b));
    end
    
    nbTargets = length(b);
    A = A*eye(nbTargets);
    b_mat = zeros(nbTargets,nbTargets);
    for tg = 1:nbTargets
        b_mat(tg,:) = circshift(b,tg-1);
    end
    y_pred = zeros(size(behavior));

    state = zeros(nbTargets,length(behavior));
    
    for s = 1:nbTargets
        state(s,1) = X0(s);
    end
    
    c = zeros(nbTargets,length(behavior));
    for t = 1:length(behavior)
        c(target(t),t) = 1;
    end
    
    y_pred(1) = c(:,1)'*state(:,1);

    for t = 2:length(behavior)
        state(:,t) = A*state(:,t-1) + b_mat*c(:,t)*(perturb(t) - y_pred(t-1));
        y_pred(t) = (c(:,t)')*state(:,t);
    end
end