% Author: Lucas Struber
% Email: lucas.struber@univ-grenoble-alpes.fr
% Institution: University Grenoble Alpes
% Lab: TIMC Laboratory
% Advisor: Fabien Cignetti
% Date: June 28, 2021
% Version: 1.0
%
% Summary: example use of state_space_fit function

% load behavioral data
clear all, close all, clc;
load('example_behavior') % 80 trials x 3 subjects
behavior = perturb - measured_error;

% Parameters initialization
%               a0  b0  x00
params_init   = [0.9, 0.1, 0];
%                amin amax bmin bmax x0min x0max
search_space   = [0.5,   1,   0, 0.3,  -15, 15];

% init fit outputs
nbSub = size(measured_error,2);
nbTrials = size(measured_error,1);

output = zeros(nbTrials,nbSub);

retentionFactor = zeros(1,nbSub); % Retention factor A
generalizationFunction = zeros(nbTargets,nbSub); % Generalization function B
initialStates = zeros(nbTargets,nbSub); % Initial states X0
noises = zeros(2,nbSub); % 2 noises (EM algorithm), sigmau and sigmax
internalStates = zeros(8,nbTrials,nbSub); % one state per direction
perf = zeros(2,nbSub); % 2 performances indicators, AICc & MSE

% loop on the number of subjects
for s = 1:nbSub
    behavior(:,s) = smooth(behavior(:,s),2); % eventually interpolate missing values

    % Fit a one-state model without retention factor (Diedrieschen, 2003),
    % using a deterministic lmse approach
    [output(:,s), retentionFactor(:,s), generalizationFunction(:,s), initialStates(:,s), noises(:,s), internalStates(:,:,s), perf(:,s)] = ...
    dir_state_space_fit(behavior(:,s), perturb(:,s), targets(:,s), nbTargets, params_init, search_space, 'em', 'norm');

    figure();
    hold on;
    title(['Subject ' num2str(s)]);
    plot(behavior(:,s), 'LineWidth',1);
    plot(output(:,s), 'LineWidth',2);
    legend('Observed error','Model output');
    hold off;
    ylabel('Angle (�)');
    xlabel('Trials');
end