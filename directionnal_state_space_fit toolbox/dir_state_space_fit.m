function [output, A, b, x0, noises_var, states, performances] = ...
    dir_state_space_fit(behavior, perturb, targets, nbTargets, params_init, params_search_ranges, fit_method, search_method)
% Author: Lucas Struber
% Email: lucas.struber@univ-grenoble-alpes.fr
% Institution: University Grenoble Alpes
% Lab: TIMC Laboratory
% Advisor: Fabien Cignetti
% Date: June 28, 2021
% Version: 1.0
%
% Summary: fit a state-space model to experimental data
%
% Input description:
%    behavior: the motor output on each trial.
%       size = nbTrials x 1
%    perturb: the perturbation on each trial.
%       size = nbTrials x 1
%    target: the target on each trial
%       size = nbTrials x 1
%    params_init: an initial guess of the parameters that seed the
%       optimization algorithm (A, b0 and X0)
%    params_search_ranges: a matrix containing upper and lower bounds for 
%       the model parameters  - 6 bounds [Amin, Amax,
%       bmin, bmax, x0min, x0max]
%    fit_method: a string that define the optimization algorithm that is
%       used (and the stochastic/deterministic nature of the modeling).
%       possible values: 'lmse' or 'em' (i.e. least mean square error
%       estimator or expectation-maximization algorithm)
%       Credits: provided directionnal EM toolbox has been adapted from
%       Scott Albert toolbox avalaible at http://shadmehrlab.org/tools
%    search_method: a string that define the form of the search space for 
%       optimization algorithm.
%       possible values: 'norm' or 'log'. If set to "log", it considers
%       that provided parameters must be converted in the 0-1 log space.
%
% Output description:
%    output: model output
%       size = nbTrials x 1
%    A: optimized retention factor
%       size = 1 x 1
%    B: optimized generalization function
%       size = nbTargets x 1 (example for 8 targets: b0 b45 b90 b135 b180
%       b-135 b-90 b-45)
%    X0: optimized initial states
%       size = nbTargets x 1
%    noises_var: extracted noises of the model. If fit_method is 'lmse',
%       only one noise is considered, between model and measurements
%       (sigmam), and if fit_method is em, two noises are optimized 
%       through Kalman filter and MLE procedure, sigmau, sigmax (motor
%       noise and state noise variances)
%       size = 1 x 2
%    states: states' vector
%       size = nbTrials x nbTargets
%    performances: model performances, characterized by two classic
%       indicators: AICc (corrected Akaike information criteria) and MSE
%       (mean square error)
%       size = 1 x 2

    addpath(strcat(fileparts(which('dir_state_space_fit.m')),'\dir_EM-toolbox'))
    
    % function need at least the behavior and the perturbation
    if nargin < 3
        error('error: this function need at least the behavior, the perturbation and the target list');
    end

    % default value if initial guess is not specified (implies one state model)
    if nargin < 4
        params_init = [0.9, 0.1, 0];
    end

    % retrieve number of states from initial guess
    nbStates = size(params_init,1);
    if nbStates ~= 1
        error('error: this function only deals with single state-spaces models');
    end

    if size(params_init,2) < 2
        error('error: params_init must contain at least A and b initial guess');
        % if x0_init is not specified, set it to 0
    elseif size(params_init,2) < 3
        params_init(1,3) = 0;
    end

    % default value for search ranges (0 < A < 1, 0 < B < 1 and X0 = 0)
    if nargin < 5
        params_search_ranges = [0, 1, ... % Amin - Amax
            0,1,...                       % Bmin - Bmax
            0,0];                         % x0min - x0max
    end
    if size(params_search_ranges,1) ~= 1
        error('error: the size of params_search_ranges is not compatible with single state model');
    end
    if size(params_search_ranges,2) < 4
        error('error: params_search_ranges must contain at least Amin, Amax, bmin and bmax for each state');
        % if x0 search range is not specified, set it to 0
    elseif size(params_search_ranges,2) < 6
        params_search_ranges(1,5) = 0;
        params_search_ranges(1,6) = 0;
    end

    % default fit method
    if nargin < 6
        fit_method = 'lmse';
    end

    % default search space
    if nargin < 7
        search_method = 'norm';
    end

    % check that string params are correct
    if not(strcmp(fit_method,'lmse') || strcmp(fit_method,'em'))
        warning('unrecognized fit method: set fit method to lmse')
        fit_method = 'lmse';
    end

    if not(strcmp(search_method,'norm') || strcmp(search_method,'log'))
        warning('unrecognized search method: set search method to norm')
        search_method = 'norm';
    end

    if strcmp(search_method,'log')
        params_init(:,1:2) = -log(1./params_init(:,1:2) - 1);
        params_search_ranges(:,1:4) = -log(1./params_search_ranges(:,1:4) - 1);
        
        params_init(isinf(params_init)) = sign(params_init(isinf(params_init)))*10;
        params_search_ranges(isinf(params_search_ranges)) = sign(params_search_ranges(isinf(params_search_ranges)))*10;
    end

    % number of parameters optimized (for AIC computation) + constraints
    % initialization
    nbOptParams = 0;
    
    if(params_search_ranges(1,1) ~= params_search_ranges(1,2))
        nbOptParams = nbOptParams + 1;
    end
    if(params_search_ranges(1,3) ~= params_search_ranges(1,4))
        nbOptParams = nbOptParams + nbTargets;
    end
    if(params_search_ranges(1,5) ~= params_search_ranges(1,6))
        nbOptParams = nbOptParams + 1;
    end

    if strcmp(fit_method,'lmse')
        nbOptParams = nbOptParams + 1; % sigma m
    else
        nbOptParams = nbOptParams + 2; % sigma u and sigma x
    end

    % local function to be used by fmincon that compute the mean square error
    % of a deterministic state-space given a set of parameters
    function mse = deterministic_ss_mse(S)
        model_estimate = simulate_deterministic_dir_state_space(S(1,1),S(1,2:nbTargets+1),S(1,nbTargets+2:2*nbTargets+1),perturb,behavior,targets,search_method);
        mse = mean((model_estimate - behavior).^2);
    end

    fmincon_x0 = [params_init(1,1), params_init(1,2), 0.001*ones(1,nbTargets-1), params_init(1,3), zeros(1,nbTargets-1)];
    fmincon_lb = [params_search_ranges(1,1), repmat(params_search_ranges(1,3),1,nbTargets), repmat(params_search_ranges(1,5),1,nbTargets)];
    fmincon_ub = [params_search_ranges(1,2), repmat(params_search_ranges(1,4),1,nbTargets), repmat(params_search_ranges(1,6),1,nbTargets)];
        
    if strcmp(fit_method,'lmse')
        S_opt = fmincon(@deterministic_ss_mse,fmincon_x0,[],[],[],[],fmincon_lb,fmincon_ub);
        
        [output, states] = simulate_deterministic_dir_state_space(S_opt(1,1),S_opt(1,2:nbTargets+1),S_opt(1,nbTargets+2:2*nbTargets+1),perturb,behavior,targets,search_method);

        noises_var = var(output - behavior);
        mse_opt = mean((output - behavior).^2);
        lik_opt = (length(behavior)/2)*(-mse_opt/noises_var-log(noises_var)-log(2*pi));
        
    
    elseif strcmp(fit_method,'em')
        
        
        c = zeros(nbTargets,length(behavior));
        for t = 1:length(behavior)
            c(targets(t),t) = 1;
        end
        
        [S_opt, likelihoods] = generalized_expectation_maximization(...
        [fmincon_x0, 2, 2, 5],... % Initial guess
        behavior,perturb,zeros(length(behavior),1),nan(length(behavior),1),c,... % Paradigm
        [fmincon_lb', fmincon_ub'; 0.0001,10000; 0.0001,10000; 0.0000001,10],...% Search-space
        100,search_method);
    
        [output, states] = simulate_deterministic_dir_state_space(S_opt(1,1),S_opt(1,2:nbTargets+1),S_opt(1,nbTargets+2:2*nbTargets+1),perturb,behavior,targets,search_method);

        mse_opt = mean((output - behavior).^2);
        lik_opt = max(likelihoods);
        
        noises_var = S_opt(2*nbTargets + 2:2*nbTargets + 3);
    end

    AICc = 2*nbOptParams - 2*lik_opt + (2*nbOptParams*(nbOptParams+1))/(length(behavior)-nbOptParams-1);
    performances = [AICc, mse_opt];

    A = S_opt(1,1);
    b = S_opt(1,2:nbTargets+1);
    x0 = S_opt(1,nbTargets+2:2*nbTargets+1);

    if strcmp(search_method,'log')
        A = 1./(1+exp(-A));
        b = 1./(1+exp(-b));
    end
end

