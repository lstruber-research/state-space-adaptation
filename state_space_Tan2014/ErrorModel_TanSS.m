function [y_pred, B] = ErrorModel_TanSS(A,c,Bstart,X0,NbPreviousTrials,StartIdx,ModelBeforeSwitch,Perturb,Error)


y_pred = zeros(size(Error));
state = zeros(size(Error));
B = zeros(size(Error));

state(1) = X0;

y_pred(1) = Perturb(1) - X0;
            
for t = 2:length(Error)
    if t < StartIdx
        if(ModelBeforeSwitch == "NoAdapt")
            B(t) = 0;
        else
            B(t) = Bstart;
        end
    else
        ErrorMean = mean(Error(max(1,t-NbPreviousTrials):t-1));
        ErrorVar = std(Error(max(1,t-NbPreviousTrials):t-1));
%         ErrorMean = mean(y_pred(max(1,t-NbPreviousTrials):t-1));
%         ErrorVar = std(y_pred(max(1,t-NbPreviousTrials):t-1));
        B(t) = c*(ErrorMean^2)/(ErrorVar^2);
        if(B(t) > 0.5), B(t) = 0.5; end
        if(B(t) < -0.5), B(t) = -0.5; end    
    end
    
    state(t) = A*state(t-1) + B(t)*y_pred(t-1);
    y_pred(t) = Perturb(t) - state(t);
end
end