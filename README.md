# state-space-adaptation
Contains different toolboxes to modelize datasets of human adaptation through state-space modeling (directional, with or without breaks, with lmse or expectation-maximization optimization, including statistical properties of error history...).

## Installation
These are Matlab toolboxes

## License
[MIT](https://choosealicense.com/licenses/mit/)
